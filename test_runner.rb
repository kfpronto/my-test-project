file_name = ARGV[0]
puts "=====Running Tests:: #{file_name}====="

file_path = "params/#{file_name}.txt"
puts "=====Opening:: #{file_path}====="

likelihood = -1
File.open(file_path) do |f|
  likelihood = f.read.to_i
end
puts "Got Likelihood:: #{likelihood}"

unless (1..100).include?(likelihood)
  puts "FAILURE! Likelihood must be between 1-100"
  exit(1)
end

successes = 0
failures = 0

(1..5).each do |i|
  puts "=====Test #{i}====="
  roll = rand(100)
  puts "Rolled #{roll}"
  if roll < likelihood
    puts "Passed!"
    successes += 1
  else
    puts "Failed!"
    failures += 1
  end
end

puts "=====Test Suite Completed====="
puts "Successes: #{successes}"
puts "Failures:  #{failures}"

puts ""
puts "TEST SUITE FAILURE!" if failures > 0
puts "Test Suite Success!" if failures == 0

exit(failures == 0)